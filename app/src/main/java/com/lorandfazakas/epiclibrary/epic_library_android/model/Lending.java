package com.lorandfazakas.epiclibrary.epic_library_android.model;

import java.time.LocalDate;

/**
 * Created by fazakaslorand on 2018. 03. 13..
 */

public class Lending {
    private String mTitle;
    private String mAuthor;
    private String mBarcode;
    private int overdueFee;
    private LocalDate mReturnDeadline;

    public Lending(String title, String author, String barcode, int overdueFee, LocalDate returnDeadline) {
        mTitle = title;
        mAuthor = author;
        mBarcode = barcode;
        this.overdueFee = overdueFee;
        mReturnDeadline = returnDeadline;
    }

    public Lending() {
    }

    public int getOverdueFee() {
        return overdueFee;
    }

    public void setOverdueFee(int overdueFee) {
        this.overdueFee = overdueFee;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public void setAuthor(String author) {
        mAuthor = author;
    }

    public String getBarcode() {
        return mBarcode;
    }

    public void setBarcode(String barcode) {
        mBarcode = barcode;
    }

    public LocalDate getReturnDeadline() {
        return mReturnDeadline;
    }

    public void setReturnDeadline(LocalDate returnDeadline) {
        mReturnDeadline = returnDeadline;
    }
}
