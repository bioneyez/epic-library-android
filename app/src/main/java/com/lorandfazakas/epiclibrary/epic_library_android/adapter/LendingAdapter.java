package com.lorandfazakas.epiclibrary.epic_library_android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lorandfazakas.epiclibrary.epic_library_android.R;
import com.lorandfazakas.epiclibrary.epic_library_android.model.Lending;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fazakaslorand on 2018. 03. 19..
 */

public class LendingAdapter extends RecyclerView.Adapter<LendingAdapter.LendingViewHolder>{
    private List<Lending> mLendings = new ArrayList<>();
    private Context mContext;

    public LendingAdapter(List<Lending> lendings, Context context) {
        mLendings = lendings;
        mContext = context;
    }

    @Override
    public LendingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lending_list_item, parent, false);
        LendingViewHolder viewHolder = new LendingViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(LendingViewHolder holder, int position) {
        holder.bindLending(mLendings.get(position));
    }

    @Override
    public int getItemCount() {
        return mLendings.size();
    }

    public class LendingViewHolder extends RecyclerView.ViewHolder {
        public TextView mAuthorLabel;
        public TextView mTitleLabel;
        public TextView mReturnDateLabel;

        public LendingViewHolder(View itemView) {
            super(itemView);
            mAuthorLabel = (TextView) itemView.findViewById(R.id.authorLabel);
            mTitleLabel = (TextView) itemView.findViewById(R.id.titleLabel);
            mReturnDateLabel = (TextView) itemView.findViewById(R.id.returnDateLabel);
        }

        public void bindLending(Lending lending) {
            mAuthorLabel.setText(lending.getAuthor());
            mTitleLabel.setText(lending.getTitle());
            mReturnDateLabel.setText(lending.getReturnDeadline().toString());
        }
    }
}
