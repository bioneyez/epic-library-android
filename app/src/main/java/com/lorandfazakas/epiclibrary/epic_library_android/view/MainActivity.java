package com.lorandfazakas.epiclibrary.epic_library_android.view;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.lorandfazakas.epiclibrary.epic_library_android.R;
import com.lorandfazakas.epiclibrary.epic_library_android.model.Message;

import org.springframework.http.HttpAuthentication;
import org.springframework.http.HttpBasicAuthentication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

public class MainActivity extends AbstractAsyncActivity {

    protected static final String TAG = MainActivity.class.getSimpleName();

    private String mJSessionId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initiate the request to the protected service
        final Button submitButton = (Button) findViewById(R.id.submit);
        submitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new FetchSecuredResourceTask().execute();
            }
        });
    }

    private void displayResponse(Message response) {
        Log.d(TAG, response.getText());
        Toast.makeText(this, response.getText(), Toast.LENGTH_LONG).show();
    }

    private class FetchSecuredResourceTask extends AsyncTask<Void, Void, Message> {

        private String username;

        private String password;

        @Override
        protected void onPreExecute() {
            showLoadingProgressDialog();

            // build the message object
            EditText editText = (EditText) findViewById(R.id.username);
            this.username = editText.getText().toString();

            editText = (EditText) findViewById(R.id.password);
            this.password = editText.getText().toString();
        }

        @Override
        protected Message doInBackground(Void... params) {
            final String url = getString(R.string.base_uri) + "/getmessage";

            // Populate the HTTP Basic Authentitcation header with the username and password
            HttpAuthentication authHeader = new HttpBasicAuthentication(username, password);
            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.setAuthorization(authHeader);
            requestHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

            // Create a new RestTemplate instance
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());

            try {
                // Make the network request
                Log.d(TAG, url);
                ResponseEntity<Message> response = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<Object>(requestHeaders), Message.class);

                List<String> cookies = response.getHeaders().get("Cookie");
                if (cookies == null) {
                    cookies = response.getHeaders().get("Set-Cookie");
                }
                String cookie = cookies.get(cookies.size() - 1);
                int start = cookie.indexOf('=');
                int end = cookie.indexOf(';');

                mJSessionId = cookie.substring(start + 1, end);

                return response.getBody();
            } catch (HttpClientErrorException e) {
                Log.e(TAG, e.getLocalizedMessage(), e);
                return new Message(0, e.getStatusText(), "Incorrect username or password");
            } catch (ResourceAccessException e) {
                Log.e(TAG, e.getLocalizedMessage(), e);
                return new Message(0, e.getClass().getSimpleName(), "Could not connect to the server");
            }
        }

        @Override
        protected void onPostExecute(Message result) {
            dismissProgressDialog();

            if (result.getSubject().equals("success")) {
                Intent intent = new Intent(MainActivity.this, MyLendingsActivity.class);
                intent.putExtra("JSESSIONID", mJSessionId);
                startActivity(intent);
            } else {
                displayResponse(result);
            }

        }

    }

}