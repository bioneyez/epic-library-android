package com.lorandfazakas.epiclibrary.epic_library_android.view;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.lorandfazakas.epiclibrary.epic_library_android.R;
import com.lorandfazakas.epiclibrary.epic_library_android.adapter.LendingAdapter;
import com.lorandfazakas.epiclibrary.epic_library_android.model.Lending;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MyLendingsActivity extends AppCompatActivity {

    public static final String TAG = MyLendingsActivity.class.getSimpleName();

    private List<Lending> mLendings;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_lendings);
        ButterKnife.bind(this);
        getLendings();
        //Toast.makeText(this, getIntent().getStringExtra("JSessionId"), Toast.LENGTH_LONG).show();

        // RECYCLER VIEW SETTING
//        LendingAdapter adapter = new LendingAdapter(mLendings,this);
//        mRecyclerView.setAdapter(adapter);
//
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
//        mRecyclerView.setLayoutManager(layoutManager);
//
//        // if no of elements fixed size
//        mRecyclerView.setHasFixedSize(true);
    }

    private void getLendings() {
        String lendingsUrl = getString(R.string.base_uri) + "/getmylendings";
        if (isNetworkAvailable()) {
            //toggleRefresh();
            String jsessionId = getIntent().getStringExtra("JSESSIONID");
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(lendingsUrl)
                    .header("Cookie", "JSESSIONID=" + jsessionId)
                    .build();
            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    //TODO
                    alertUserAboutError();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    // TODO FINISH
                    try {
                        String jsonData = response.body().string();
                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {
                            mLendings = parseLendings(jsonData);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
//                                    updateDisplay();
                                    LendingAdapter adapter = new LendingAdapter(mLendings,MyLendingsActivity.this);
                                    mRecyclerView.setAdapter(adapter);

                                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MyLendingsActivity.this);
                                    mRecyclerView.setLayoutManager(layoutManager);

                                    // if no of elements fixed size
                                    mRecyclerView.setHasFixedSize(true);
                                }
                            });
                        } else {
                            alertUserAboutError();
                        }
                    } catch (IOException e) {
                        Log.e(TAG, "Exception caught: ", e);
                    } catch (JSONException e) {
                        Log.e(TAG, "Exception caught: ", e);
                    }

                }
            });
        }
    }

    private List<Lending> parseLendings(String jsonData) throws JSONException {
        JSONObject root = new JSONObject(jsonData);
        JSONArray lendingsJsonArray = root.getJSONArray("lendings");
        List<Lending> lendings = new ArrayList<>();
        for (int i = 0; i < lendingsJsonArray.length(); i++) {
            lendings.add(parseLendingDetails(lendingsJsonArray.getJSONObject(i)));
        }
        return lendings;
    }

    private Lending parseLendingDetails(JSONObject lendingObject) throws JSONException{
        Lending lending = new Lending();
        //JSONObject lendingObject = new JSONObject(jsonData);
        String title = lendingObject.getString("title");
        String author = lendingObject.getString("author");
        int overdueFee = lendingObject.getInt("overdueFee");
        String barcode = lendingObject.getString("barcode");
        JSONObject returnDeadlineObject = lendingObject.getJSONObject("returnDeadline");
        int year = returnDeadlineObject.getInt("year");
        int month = returnDeadlineObject.getInt("monthValue");
        int day = returnDeadlineObject.getInt("dayOfMonth");
        lending.setAuthor(author);
        lending.setTitle(title);
        lending.setOverdueFee(overdueFee);
        lending.setBarcode(barcode);
        lending.setReturnDeadline(LocalDate.of(year,month,day));
        return lending;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;
        if (networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;
        }
        return isAvailable;
    }

    private void alertUserAboutError() {
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.show(getFragmentManager(), "error_dialog");
    }
}
